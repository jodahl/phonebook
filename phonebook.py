__author__ = 'Josiah Dahl'

import xlrd
import traceback
import reportlab.rl_config
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFontFamily
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY, TA_LEFT, TA_RIGHT
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Paragraph, Frame, BaseDocTemplate, NextPageTemplate, PageBreak, PageTemplate,\
    FrameBreak, Image, Table, TableStyle

# Define fonts
reportlab.rl_config.warnOnMissingFontGlyphs = 0
pdfmetrics.registerFont(TTFont('Georgia', 'fonts/GEORGIA.ttf'))
pdfmetrics.registerFont(TTFont('GeorgiaB', 'fonts/GEORGIAB.ttf'))
registerFontFamily('Georgia',normal='Georgia', bold='GeorgiaB')


def read_file(filename):
    """
    :param filename: The name of the file to open
    :return: tuple of (list of field names, list of all the rows in the file)
    """
    try:
        workbook = xlrd.open_workbook(filename)
        worksheet = workbook.sheet_by_name(workbook.sheet_names()[0])
        num_rows = worksheet.nrows
        num_cells = worksheet.ncols
        fields = []
        entries = []
        # Parse the rows
        curr_row = 0
        while curr_row < num_rows:
            row_data = []
            for cell in xrange(num_cells):
                row_data.append(worksheet.cell_value(curr_row, cell))
            if curr_row == 0:
                fields = row_data
            else:
                entries.append(row_data)
            curr_row += 1
        return fields, entries
    except Exception, err:
        print traceback.format_exc()

def parse_rows(fields, entries):
    """
    :param row: A row (not the first one) that contains member information
    :return: A list of dictionaries of column values : row values for that row
    """
    formatted_entries = []

    for entry in entries:
        entry_dict = {}
        for field in xrange(len(fields)):
            entry_dict[fields[field]] = entry[field]

        formatted_entries.append(entry_dict)
    return formatted_entries

def create_document(formatted_entries, fields, filename, folder, display_empty_photos):
    width, height = letter

    # Styles
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='member_name_style', fontName='Georgia', fontSize=14, alignment=TA_CENTER, spaceAfter=4))
    styles.add(ParagraphStyle(name='member_style', fontName='Georgia', fontSize=12, alignment=TA_CENTER, spaceAfter=2))

    Elements = []
    doc = BaseDocTemplate(filename, pagesize=letter, leftMargin=0.75*inch, rightMargin=0.75*inch, topMargin=inch,
                          bottomMargin=inch)
    def page_num_footer(canvas, doc):
        canvas.saveState()
        canvas.setFont('Georgia',10)
        canvas.drawString(width / 2, 0.75 * inch, "{0}".format(doc.page - 1))
        canvas.restoreState()

    def title_page(canvas, doc):
        canvas.saveState()
        canvas.setFont('GeorgiaB', 24)
        logo_width = 2.34 * 3 * inch
        logo_height = 3 * inch
        title_text = 'PHOTO DIRECTORY'
        year_text = '2015'
        canvas.drawCentredString(width/2, 5.5 * inch, title_text)
        canvas.drawCentredString(width/2, 3.5 * inch, year_text)
        canvas.restoreState()

    # Frames
    # Title page declarations)
    lg_name = Frame(doc.leftMargin, doc.bottomMargin + 5.5  * inch, letter[0], 72, leftPadding=0,bottomPadding=0,
                       rightPadding=0,topPadding=0, id='top_right',showBoundary=0)
    top_left = Frame(doc.leftMargin, doc.bottomMargin + 6 * inch, 3.5 * inch, 3 * inch, leftPadding=0,bottomPadding=0,
                     rightPadding=0,topPadding=0, id='top_left',showBoundary=0)
    top_right = Frame(doc.leftMargin + 3.5 * inch, doc.bottomMargin + 6 * inch, 3.5 * inch, 3 * inch, leftPadding=0,bottomPadding=0,
                      rightPadding=0,topPadding=0, id='top_right',showBoundary=0)
    mid_left = Frame(doc.leftMargin, doc.bottomMargin + 3 * inch, 3.5 * inch, 3 * inch, leftPadding=0,bottomPadding=0,
                     rightPadding=0,topPadding=0, id='top_left',showBoundary=0)
    mid_right = Frame(doc.leftMargin + 3.5 * inch, doc.bottomMargin + 3 * inch, 3.5 * inch, 3 * inch, leftPadding=0,bottomPadding=0,
                      rightPadding=0,topPadding=0, id='top_right',showBoundary=0)
    bottom_left = Frame(doc.leftMargin, doc.bottomMargin, 3.5 * inch, 3 * inch, leftPadding=0,bottomPadding=0,
                        rightPadding=0,topPadding=0, id='bottom_left',showBoundary=0)
    bottom_right = Frame(doc.leftMargin + 3.5 * inch, doc.bottomMargin, 3.5 * inch, 3 * inch, leftPadding=0,bottomPadding=0,
                         rightPadding=0,topPadding=0, id='bottom_right',showBoundary=0)

    # Page Templates
    doc.addPageTemplates([PageTemplate(id='title_page', frames=[lg_name], onPage=title_page)])
    doc.addPageTemplates([PageTemplate(id='normal', frames=[Frame(doc.leftMargin, doc.bottomMargin,
                                                                  7 * inch, 9 * inch)], onPage=page_num_footer)])
    doc.addPageTemplates([PageTemplate(id='six_box', frames=[top_left, top_right, mid_left, mid_right, bottom_left,
                                                             bottom_right], onPage=page_num_footer)])

    Elements.append(NextPageTemplate('title_page'))
    # Frame Declarations

    entry_count = 1
    Elements.append(NextPageTemplate('six_box'))
    # field_list = [u'Name', u'Children', u'Phone', u'Cell Phone 1', u'Cell Phone 2', u'Email 1', u'Email 2']
    # [u'Name', u'Children', u'Phone', u'Cell Phone 1', u'Cell Phone 2', u'Email 1', u'Email 2', u'Photo Filename', u'Photo']
    for entry in formatted_entries:
        # Check if display_no_photos is active, otherwise will not show people with no photo
        if not display_empty_photos and entry['Photo Filename'] == '':
            continue

        if entry['Photo Filename'] != '':
            try:
                photo = Image(folder + '/' + entry['Photo Filename'], width=2.5*inch, height=2*inch)
            except IOError:
                # For photos labeled filename.jpg.jpg
                try:
                    photo = Image(folder + '/' + entry['Photo Filename'] + '.jpg', width=2.5*inch, height=2*inch)
                except:
                    if not display_empty_photos:
                        continue
                    else:
                        photo = Image('photos/no_picture.jpg', width=2.5*inch, height=2*inch)
        else:
            photo = Image('photos/no_picture.jpg', width=2.5*inch, height=2*inch)
        photo.hAlign = 'CENTER'
        Elements.append(photo)
        field_labels = [u'Name', u'Children']
        for field_label in fields:
            if entry[field_label] != '' and field_label in field_labels:
                if field_label == u'Name':
                    Elements.append(Paragraph('<b>' + entry[field_label] + '</b>', styles['member_name_style']))
                else:
                    Elements.append(Paragraph(entry[field_label], styles['member_style']))
        Elements.append(FrameBreak())

    ################################
    # Now for the Index at the end #
    ################################

    # Break to the index page
    # Table(data, colWidths=None, rowHeights=None, style=None, splitByRow=1, repeatRows=0, repeatCols=0)
    table_data = []
    table_style = TableStyle([('FONT',(0, 0), (-1, -1), 'Georgia', 10), ('LINEABOVE', (0, 1), (-1, -1), 1, (0, 0, 0)),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')])
    for entry in formatted_entries:
        field_labels = [u'Name', u'Phone', u'Cell Phone 1', u'Cell Phone 2', u'Email 1', u'Email 2']
        row = []
        # Cap name at 30 characters, split by closest space <= to 30
        name = entry[u'Name']
        if len(name) > 30:
            for i in range(30, -1, -1):
                if name[i] == ' ':
                    name = name[:i] + '\n' + name[i:]
                    break
        row.append(name)
        phone = ''
        email = ''
        if entry[u'Phone'] != '':
            phone += entry[u'Phone']
        if entry[u'Cell Phone 1'] != '':
            # For if there is no home phone but a cell phone
            if phone != '':
                phone += '\n' + entry[u'Cell Phone 1']
            else:
                phone += entry[u'Cell Phone 1']
        if entry[u'Cell Phone 2'] != '':
            phone += '\n' + entry[u'Cell Phone 2']
        row.append(phone)
        e = entry[u'Email 1'] + ';' + entry[u'Email 2']
        e_split = e.split(';')
        email += e_split[0]
        for each in e_split[1:]:
            if each != '':
                email += '\n' + each
        row.append(email)
        table_data.append(row)
    index = Table(table_data, colWidths=None, rowHeights=None, style=table_style, splitByRow=1, repeatRows=0,
                  repeatCols=0)
    index_header = Paragraph('INDEX OF PEOPLE\n\n\n', styles['member_name_style'])

    Elements.append(NextPageTemplate('normal'))
    Elements.append(PageBreak())
    Elements.append(index_header)
    Elements.append(index)
    doc.build(Elements)

def main():

    default_filename = 'PhotoDirectoryList.xlsx'
    default_export_filename = "PhotoDirectory.pdf"
    default_folder = 'photos'
    display_empty_photos = False

    filename = raw_input('Please enter the filename or leave blank to use %s: ' % default_filename)
    export_filename = raw_input('Please enter the filename you wish to save it as or leave blank to save as %s: ' % default_export_filename)
    folder = raw_input('Please enter the folder the files are in or leave blank to use %s/: ' % default_folder)
    if filename == '':
        filename = default_filename
    if export_filename == '':
        export_filename = default_export_filename
    if folder == '':
        folder = default_folder
    try:
        fields, entries = read_file(filename)
        formatted_entries = parse_rows(fields, entries)
    except:
        print "Opening {0} failed".format(filename)
    try:
        create_document(formatted_entries, fields, export_filename, folder, display_empty_photos)
        print '{0} created successfully!'.format(export_filename)
    except Exception, err:
        print err
        print 'Something when wrong, creation of {0} failed'.format(export_filename)



if __name__ == "__main__":
    main()