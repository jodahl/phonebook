# README #

This application was developed to create a simple photo directory with a phone book based on information from an Excel file.

### Usage ###

![usage.png](https://bitbucket.org/repo/MekyRz/images/1252940749-usage.png)

### How do I get set up? ###

Required Python packages are:

* [xlrd](https://pypi.python.org/pypi/xlrd)
* [reportlab](https://pypi.python.org/pypi/reportlab/3.1.44)

The headings for the Excel file are as follows:

Name, Children, Phone, Cell Phone 1, Cell Phone 2, Email 1, Email 2, Photo Filename.

Photos should be saved in a "photos" folder, with the same filename as specified in the Excel file. Run the program as shown in "Usage" above.

### Output ###
![photopage.png](https://bitbucket.org/repo/MekyRz/images/158831643-photopage.png)
![index.png](https://bitbucket.org/repo/MekyRz/images/4086933981-index.png)